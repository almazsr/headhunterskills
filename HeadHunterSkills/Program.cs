﻿using AngleSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace HeadHunterSkills
{
    class Program
    {
        static IBrowsingContext Context = BrowsingContext.New(Configuration.Default.WithDefaultLoader());

        static async Task Main(string[] args)
        {
            var uriTemplate = "https://kazan.hh.ru/search/vacancy?st=searchVacancy&text=%D0%9F%D1%80%D0%BE%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B8%D1%81%D1%82+c%23&salary=&currency_code=RUR&experience=doesNotMatter&order_by=relevance&search_period=0&items_on_page=100&no_magic=true&L_save_area=true&page=1";

            const int maxPage = 19;
            var skills = new Dictionary<string, int>();
            for (int page = 0; page <= maxPage; page++)
            {
                var uri = string.Format(uriTemplate, page);
                var document = await Context.OpenAsync(uri);
                var vacanciesCssSelector = "a.bloko-link[data-qa=vacancy-serp__vacancy-title]";

                var vacancyElements = document.QuerySelectorAll(vacanciesCssSelector).ToArray();
                var vacancyUris = vacancyElements.Select(v => v.GetAttribute("href")).ToArray();
                foreach (var vacancyUri in vacancyUris)
                {
                    var vacancySkills = await GetSkills(vacancyUri);
                    foreach (var vacancySkill in vacancySkills)
                    {
                        if (!skills.ContainsKey(vacancySkill))
                        {
                            skills.Add(vacancySkill, 1);
                        }
                        else
                        {
                            skills[vacancySkill] += 1;
                        }
                    }
                }
                File.WriteAllLines("skills.txt", skills.Select(s => $"{s.Key}\t{s.Value}"));
            }
        }

        static async Task<string[]> GetSkills(string uri)
        {
            var document = await Context.OpenAsync(uri);
            var skillsCssSelector = "span[data-qa=bloko-tag__text]";
            var skillElements = document.QuerySelectorAll(skillsCssSelector);
            return skillElements.Select(m => m.TextContent).ToArray();
        }
    }
}
